const Discord = require('discord.js');
const Bot = new Discord.Client();
const Path = require('path')
const fs = require('fs')

const token = /* Your token here */'';
const botPrefix = '--';

Bot.on('ready', () => {
    console.log('UberBennaBot online!');
});

Bot.on('message', msg => {

    let checkPrefix = msg.content.substr(0, 2);
    let args = msg.content.slice(2).split(' ');
    let command = args[0];

    if(checkPrefix === botPrefix){

        const path = `./commands/${command}.js`
    
        try{

            if(fs.existsSync(path)){
                let url = Path.resolve(__dirname, path);
                let commandClass = require(url);
                let Command = new commandClass(Bot, args, msg);
                Command.execute()
            }

        }
        catch(err){
            console.log(err)
        }

    }

});

Bot.login(token);
