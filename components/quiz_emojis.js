const qe = [
    {
        'label': 'orange_circle',
        'emoji': '🟠'
    },
    {
        'label': 'yellow_circle',
        'emoji': '🟡'
    },
    {
        'label': 'green_circle',
        'emoji': '🟢'
    },
    {
        'label': 'purple_circle',
        'emoji': '🟣'
    }
];

module.exports = qe;