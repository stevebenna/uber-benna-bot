const categories = {
    'general': {
        'command': 'general',
        'id': null,
        'label': 'Qualsiasi cosa'
    },
    'videogames': {
        'command': 'videogames',
        'id': '15',
        'label': 'Videogiochi'
    },
    'computer':{
        'command': 'computer',
        'id': '18',
        'label': 'Computer'
    },
    'tech':{
        'command': 'tech',
        'id': '30',
        'label': 'Tecnologia'
    },
    'japan':{
        'command': 'japan',
        'id': '31',
        'label': 'Anime e manga'
    }
};

module.exports = categories;