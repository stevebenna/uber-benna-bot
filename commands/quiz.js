const Command = require('./command');

class Quiz extends Command {

    execute(){

        const categories = require('../components/quiz_categories.js');
        const quiz_emojis = require('../components/quiz_emojis.js');
        const fetch = require('node-fetch');
        const quizFileLocation = __dirname+'/../files/quiz.txt';

        this.categories = categories;
        this.qe = quiz_emojis;
        this.fetch = fetch;
        this.players = {};

        this.category = typeof(this.args[0]) !== 'undefined' && this.args[0] in this.categories ? this.categories[this.args[0]] : this.categories['general'];
        this.loops = typeof(this.args[1]) !== 'undefined' ? this.args[1] : 5;
        
        this.quizFileLocation = quizFileLocation;

        this.resultChart('create');
        this.getQuestion();

    }

    getQuestion(){

        let msg = `L'argomento del quiz sarà "${this.category.label}"`;
        this.channel.send(msg);

        this.fetch(`https://opentdb.com/api.php?amount=1&type=multiple`+(this.category.command != 'general' ? `&category=${this.category.id}` : ''), {
            method: 'get'
        })
        .then((response) => {
            return response.json();
        })
        .then((jsonData) => {
            this.formatQuestion(jsonData.results);
            this.printQuestion();
        });

    }

    formatQuestion(question){

        const _ = require('lodash');

        this.question = question[0];

        let answers = [...this.question.incorrect_answers];
        answers.push(this.question.correct_answer);
        let shuffled = _.shuffle(answers);
        let shuffledAnswers = [];

        shuffled.forEach((answer, index) => {
            shuffledAnswers[index] = {
                'emoji': this.qe[index].label,
                'emojiId': escape(this.qe[index].emoji),
                'answer': answer,
                'isCorrect': answer === this.question.correct_answer
            }
        });

        this.question.answers = [...shuffledAnswers];
        this.question.correct_answer = this.question.answers.filter(answer => answer.isCorrect === true)[0];

    }

    printQuestion() {

        const validReactions = this.qe.map(emoji => emoji.emoji).map(escape);
        const filter = (reaction, user) => {    
            let reactionId = escape(reaction.emoji.name);        
            if(validReactions.includes(reactionId) && user.id !== msgToReact.author.id){
                if(user.username in this.players){
                    return false;
                }
                
                this.players[user.username] = reactionId;
                return true;
            }
            return false;
        }

        let msg;
        let msgToReact;
        let usersWhoAnsweredCorrectly = [];

        msg = `Domanda\n${this.convertChars(this.question.question)}\n\n`
        this.question.answers.forEach((answer) => {
            msg += `:${answer.emoji}: ${this.convertChars(answer.answer)}\n`;
        });

        this.channel.send(msg)
            .then(message => {
                msgToReact = message;
                this.qe.forEach(emoji => {
                    msgToReact.react(emoji.emoji);
                });
                msgToReact.awaitReactions(filter, { time: 20000 })
                    .then(collected => {
                        const reactions = collected;
                        this.channel.send(`\nLa risposta corretta è :${this.question.correct_answer.emoji}: ${this.question.correct_answer.answer}\n`);

                        for(let author in this.players){
                            if(this.players[author] == this.question.correct_answer.emojiId){
                                usersWhoAnsweredCorrectly.push(`@${author}`);
                                this.resultChart('add', author);
                            }
                        }

                        this.channel.send('\n'+usersWhoAnsweredCorrectly.length > 0 ? `Hanno risposto correttamente: ${usersWhoAnsweredCorrectly.join(', ')}` : 'Nessuno ha risposto correttamente');
                        this.loops = this.loops -1;

                        if(this.loops > 0){
                            this.channel.send(`--quiz ${this.category.command} ${this.loops} false`);
                        }
                        else{
                            let finalChart = this.resultChart('get');
                            this.channel.send(`Quiz terminato, ecco i risultati:\n\n${finalChart}`);
                            this.fs.unlinkSync(this.quizFileLocation);
                            this.channel.send(`--end`);
                        }

                    })
                    .catch(err => {
                        console.log(err);
                        console.log('catch');
                    });
            });

    }

    resultChart(command, content = null){

        switch(command){

            case 'create':
                if(!this.fs.existsSync(this.quizFileLocation)){
                    this.fs.writeFileSync(this.quizFileLocation, '', (err) => {
                        console.log(err ? err : 'Chart created!');
                    });
                }
                break;

            case 'add':
                this.fs.appendFileSync(this.quizFileLocation, content+';');
                break;

            case 'get':

                try{
                    let data = this.fs.readFileSync(this.quizFileLocation, 'utf-8');
                    let chart = [];
                    let out = '';
                    let points = data.split(';');

                    if(points.length > 0){

                        points.forEach(point => {
                            if(point != ''){
    
                                if(point in chart){
                                    chart[point] = chart[point]+1;
                                }
                                else{
                                    chart[point] = 1;
                                }
                            }
                        });

                        for(let author in chart){
                            out = out+`${author} - ${chart[author]} pts\n`;
                        }
                        return out;
                    }
                    else return false;

                }
                catch(err){
                    console.log(err);
                }

                break;

            default:
                console.log('No command found!');
                break;

        }

    }

}

module.exports = Quiz;