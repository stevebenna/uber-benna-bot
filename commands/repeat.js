const Command = require('./command');

class Repeat extends Command {

    execute(){
        this.channel.send(this.args.join(' '));
    }

}

module.exports = Repeat;
