const Command = require('./command');

class Danbooru extends Command {

    execute(){

        const _ = require('lodash');
        const danbooruUrl = 'https://danbooru.donmai.us/posts/';
        const token = _.random(4, 2100000);
        const image = danbooruUrl+token;
        
        this.channel.send(`Danbooru evocato da <@${this.author.id}>`);
        this.channel.send(image);

    }

}

module.exports = Danbooru;