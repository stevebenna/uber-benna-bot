const Command = require('./command')

class Shuffle extends Command {

    async execute() {
        try{
            const shuffled = this.mentions.users.first()
            await this.channel.send(`Haha, <@${shuffled.id}> goes brr`)
            const voice = this.guild.member(shuffled).voice
            const channels = require('../components/voice_channels.js');
            const iterations = parseInt(this.args[1]) || 10
            let counter = 0
            const length = channels.length - 1
    
            for(let i = 0; i < iterations; ++i){
                voice.setChannel(channels[counter])
                ++counter
                if(counter > length){
                    counter = 0
                }
            }
        }
        catch(err) {
            this.channel.send(`Nessun utente con quel nome collegato`)
        }

    }

}

module.exports = Shuffle