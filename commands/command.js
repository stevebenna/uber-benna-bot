
class Command {
    
    constructor(Bot, args, message) {

        const defaultChannel = require('../components/default_channel.js');
        const channels = require('../components/channels.js');
        const fs = require('fs');

        this.Bot = Bot;
        this.args = args.filter((value, index) => {
            if(index > 0 && value.trim() != ''){
                return value;
            }
        });

        this.fs = fs;
        this.defaultChannel = defaultChannel;
        this.channels = channels;
        this.channel = message.channel;
        this.author = message.author;
        this.guild = message.guild;
        this.mentions = message.mentions
        this.member = message.member

    }

    printArgs(){

        this.args.forEach((value, key) => {
            this.channel.send(`${key} - ${value}\n`);
        });

    }

    execute(){
        this.printArgs();
    }

    setChannel(channelName = 'bot_spam'){
        
        let msg = null;

        if(!channelName in this.channels){
            msg = `Canale non trovato, mi collego a ${this.defaultChannel}`;
            channelName = this.defaultChannel;
        }

        this.channel = this.Bot.channels.cache.get(this.channels[channelName]);

        if(msg){
            this.channel.send(msg);
        }

    }

    convertChars(string){

        return string.replace(/&quot;/g, "\"")
                .replace(/&aacute;/g, "á")
                .replace(/&agrave;/g, "à")
                .replace(/&Aacute;/g, "Á")
                .replace(/&Agrave;/g, "À")
                .replace(/&eacute;/g, "é")
                .replace(/&egrave;/g, "è")
                .replace(/&Eacute;/g, "É")
                .replace(/&Egrave;/g, "È")
                .replace(/&iacute;/g, "í")
                .replace(/&igrave;/g, "ì")
                .replace(/&Iacute;/g, "Í")
                .replace(/&Igrave;/g, "Ì")
                .replace(/&oacute;/g, "ó")
                .replace(/&ograve;/g, "ò")
                .replace(/&Oacute;/g, "Ó")
                .replace(/&Ograve;/g, "Ò")
                .replace(/&uacute;/g, "ú")
                .replace(/&ugrave;/g, "ù")
                .replace(/&Uacute;/g, "Ú")
                .replace(/&Ugrave;/g, "Ù")
                .replace(/&#039;/g, '\'')
                .replace(/&amp;/g, '&')
                .replace(/&uuml/g, 'ü');

    }

}

module.exports = Command;