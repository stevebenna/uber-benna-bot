const Command = require('./command')
const Discord = require('discord.js')

class Covid extends Command {

    execute() {

        this.scoreboard = `${__dirname}/../files/covid.json`
        this.defaultColor = 'DARK_NAVY'
        this._ = require('lodash');
        this.win = 40

        const allowedCommands = ['login', 'init', 'play', 'next', 'ready']
        
        if(typeof(this.args[0]) === 'undefined' || !allowedCommands.includes(this.args[0])){
            this.channel.send(`Comando non riconosciuto`)    
        }
        else{
            const fnc = this.args[0]
            this[fnc]()
        }

    }

    async init(){

        const init = {pars: {currentOrder: 1, nextFreeKey: 1, isReady: false}, users:{}}

        this.fs.writeFileSync(this.scoreboard, JSON.stringify(init), (err) => {
            console.warn(err ? err : `Tabella punteggi creata correttamente`)
        })

        const embed = new Discord.MessageEmbed()
                        .setTitle('Gioco del covid')
                        .setColor('DARK_NAVY')
                        .setDescription(`di seguito i comandi:
                                        --covid login per registrarsi
                                        --covid ready per confermare i giocatori e cominciare a giocare
                                        --covid play per giocare (ÜberBennaBot chiamerà a turno)`)
        this.channel.send(embed)

    }

    async login(){

        const {id} = this.author

        const scoreboard = await this.getScoreboard()

        if(scoreboard['users'][id]){
            this.channel.send(`<@${id}> è già registrato!`)
        }
        else if(scoreboard.pars.isReady !== false){
            this.channel.send(`Impossibile registrarsi, il gioco è già cominciato!`)
        }
        else{
            this.initUser(scoreboard, id)
                .then(() => {
                    this.channel.send(`<@${id}> registrato correttamente`)
                })
                .catch(err => {
                    this.channel.send(`errore generico`)
                })
        }

    }

    ready(){

        this.getScoreboard()
            .then(scoreboard => {

                scoreboard.pars.isReady = true
                this.saveScoreboard(scoreboard)
                    .then(() => {
                        const pic = this.createImg('0').setTitle(`Parte il gioco!`)
                        this.channel.send(pic)
                            .then(() => {
                                this.callNextUser()
                            })
                            .catch(err => {
                                console.log(`Impossibile giocare`)
                                console.log(err)
                            })
                    })
                    .catch(err => {
                        console.log(`Impossibile salvare la scoreboard`)
                        console.log(err)
                    })                    
                
            })

    }

    async play(){
        try{
            const scoreboard = await this.getScoreboard()
            if(scoreboard.users[this.author.id]){
                const whoPlays = scoreboard.users[this.author.id]
                if(whoPlays.yourTurn === false){
                    await this.channel.send(`Aspetta, non è il tuo turno!`)
                }
                else{
                    const limit = whoPlays.cell > 34 ? 6 : 10
                    await this.channel.send(`<@${this.author.id}> lancerà un dado da ${limit}`)
                    const result = await this.throwDice(limit)
                    await this.channel.send(`<@${this.author.id}> ha tirato ${result}`)
                    let newCell = whoPlays.cell + result

                    if(newCell > this.win) {
                        newCell = this.win - (newCell - this.win)
                    }

                    if(newCell === this.win){
                        await this.channel.send(`<@${this.author.id}> ha vinto!`)
                    }
                    if(newCell < this.win){
                        let embed = this.createImg(newCell)
                                          .setDescription(`<@${this.author.id}> avanza alla cella ${newCell}`)
                        scoreboard.users[this.author.id].cell = newCell
                        await this.saveScoreboard(scoreboard)
                        await this.channel.send(embed)
                        const {msg, player} = await this.handleCellResult(whoPlays)
                        scoreboard.users[this.author.id] = player
                        await this.saveScoreboard(scoreboard)
                        await this.channel.send(msg)
                        if(newCell !== player.cell){
                            embed = this.createImg(player.cell)
                            await this.channel.send(embed)
                        }
                        this.callNextUser()
                    }

                }
            }
            else{
                this.channel.send(`E tu chi cazzo sei? Non sei registrato`)
            }
        }
        catch(err){
            console.log(`errore`)
            console.log(err)
        }

    }

    next(){
        this.callNextUser()
    }

    async callNextUser(){

        const scoreboard = await this.getScoreboard()
        const nUsers = Object.entries(scoreboard.users).length

        let curOrd = scoreboard.pars.currentOrder      
        let nextUser = null

        if(curOrd > nUsers){
            curOrd = 1
        }

        Object.entries(scoreboard.users).map(([index, user]) => {
            if(parseInt(user.orderKey) === curOrd){
                nextUser = user
            }
            else{
                scoreboard.users[index].yourTurn = false
            }
        })

        nextUser.yourTurn = true
        scoreboard.users = {...scoreboard.users, [nextUser.id]:nextUser}
        scoreboard.pars.currentOrder = curOrd+1

        this.saveScoreboard(scoreboard)
            .then(() => {
                this.channel.send(`Tocca a <@${nextUser.id}>`)
                    .then(async () => {
                        if(nextUser.yeast === true && nextUser.stops > 0){
                            --nextUser.stops
                            nextUser.yeast = false
                            await this.channel.send(`<@${nextUser.id}> usa lievito!`)
                        }
                        if(nextUser.vaccine === true && nextUser.stops > 0){
                            --nextUser.stops
                            nextUser.vaccine = false
                            await this.channel.send(`<@${nextUser.id}> usa vaccino!`)
                        }
                        if(nextUser.stops > 0){
                            const newStops = --nextUser.stops
                            scoreboard.users = {...scoreboard.users, [nextUser.id]:nextUser}
                            this.saveScoreboard(scoreboard)
                                .then(async () => {
                                    await this.channel.send(`<@${nextUser.id}> salta questo turno (${newStops} rimast${newStops == 1 ? `o` : `i`})`)
                                    this.callNextUser()
                                })
                                .catch(err => {
                                    console.log(`Impossibile salvare la scoreboard`)
                                    console.log(err)
                                })
                        }
                        if(nextUser.alive === false){
                            await this.channel.send(`<@${nextUser.id}> è morto`)
                            this.callNextUser()
                        }
                    })
                    .catch(err => {
                        console.log(`Impossibile chiamare il prossimo giocatore`)
                        console.log(err)
                    })
            })
            .catch(err => {
                console.log(`Impossibile salvare la scoreboard`)
                console.log(err)
            })

    }

    createImg(name){

        const img = new Discord.MessageAttachment(`${__dirname}/../covid/${name}.png`, `${name}.png`)
        const msg = new Discord.MessageEmbed()
                        .attachFiles(img)
                        .setImage(`attachment://${name}.png`)
                        .setColor(this.defaultColor)

        return msg

    }

    getScoreboard(){

        return new Promise((resolve, reject) => {

            this.fs.readFile(this.scoreboard, 'utf-8', (err, data) => {
                if(err){
                    reject(err)
                }
                else{
                    resolve(JSON.parse(data))
                }
            })

        })

    }

    async initUser(scoreboard, id){

        return new Promise((resolve, reject) => {

            scoreboard.users[id] = {
                id: id,
                cell: 0,
                stops: 0,
                alive: true,
                yeast: false,
                vaccine: false,
                yourTurn: false,
                orderKey: scoreboard.pars.nextFreeKey
            }

            ++scoreboard.pars.nextFreeKey

            try{
                this.saveScoreboard(scoreboard)
                    .then(() => {
                        resolve(true)
                    })
                    .catch(err => {
                        console.log(`Impossibile registrare`)
                        console.log(err)
                    })
            }
            catch(err){
                reject(err)
            }

        })

    }

    saveScoreboard(scoreboard){

        return new Promise((resolve, reject) => {

            this.fs.writeFile(this.scoreboard, JSON.stringify(scoreboard), (err, data) => {
                if(err){
                    reject(err)
                }
                else{
                    resolve(true)
                }
            })
    
        })


    }

    getSize(obj) {
        let size = 0, key;
        for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    }

    throwDice(limit){
        return new Promise((resolve, reject) => {           

            try{
                const result = this._.random(1, limit)
                resolve(result)
            }
            catch(err){
                reject(err)
            }

        })
    }

    handleCellResult(player){

        let msg = null

        switch(player.cell){

            case 1:
            case 9:
            case 23:
            case 28:
            case 31:
            case 33:
            case 35:
            case 38:
                msg = `Nessuna azione da compiere`
                break
            case 2:
            case 39:
                msg = `<@${player.id}> ricomincia dal via`
                player.cell = 0
                break
            case 3:
                msg = `<@${player.id}> deve rispondere a Zingaretti con accento svedese`
                break
            case 4:
                msg = `<@${player.id}> deve restare un turno con la mascherina sugli occhi`
                break
            case 5:
                msg = `<@${player.id}> avanza di 4 caselle`
                player.cell += 4
                break
            case 6:
            case 8:
            case 10:
            case 13:
            case 26:
            case 30:
            case 37:
                msg = `<@${player.id}> resta fermo 1 turno`
                player.stops = 1
                break
            case 7:
                msg = `<@${player.id}> deve... boh`
                break
            case 11:
            case 12:
            case 29:
                msg = `<@${player.id}> va in gallera!`
                player.cell = 8
                player.stops = 1
                break
            case 14:
                msg = `Gli altri giocatori possono menare <@${player.id}>`
                break
            case 15:
                msg = `<@${player.id}> avanza di 3 caselle`
                player.cell += 3
                break
            case 16:
                msg = `<@${player.id}> guadagna un panetto di lievito!`
                player.yeast = true
                break
            case 17:
                msg = `<@${player.id}> deve dirci quanto è ingrassat* in quarantena`
                break
            case 18:
                msg = `<@${player.id}> avanza di 2 caselle`
                player.cell += 2
                break
            case 19:
                msg = `<@${player.id}> retrocede di 10 caselle`
                player.cell -= 10
                break
            case 20:
                msg = `<@${player.id}> avanza di 5 caselle`
                player.cell += 5
                break
            case 21:
                msg = `<@${player.id}> guadagna un vaccino!`
                player.vaccine = true
                break
            case 22:
            case 36:
                msg = `<@${player.id}> avanza di 1 casella`
                player.cell += 1
                break
            case 24:
                msg = `<@${player.id}> retrocede di 15 caselle`
                player.cell -= 15
                break
            case 25:
                msg = `<@${player.id}> avanza di 7 caselle`
                player.cell += 7
                break
            case 27:
                msg = `<@${player.id}> retrocede di 3 caselle`
                player.cell -= 3
                break
            case 32:
                msg = `<@${player.id}> si ferma 4 turni`
                player.stops = 4
                break
            case 34:
                msg = `<@${player.id}> è stato silurato`
                player.alive = false
                player.stops = 0
                break
            default:
                break
        }

        return {msg, player}

    }

}

module.exports = Covid