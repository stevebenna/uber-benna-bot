const Command = require('./command');

class Roll extends Command {

    execute(){
        let nDices = typeof(this.args[0]) !== 'undefined' ? this.args[0] : 1;
        let maxValue = typeof(this.args[1]) !== 'undefined' ? this.args[1] : 6;

        if(isNaN(nDices) || isNaN(maxValue)){
            this.channel.send('Attenzione! Sono stati inseriti valori non numerici');
        }
        else{
            const _ = require('lodash');
            let stronzo = false

            nDices = parseInt(nDices)
            maxValue = parseInt(maxValue)

            if(nDices > 50){
                nDices = 50
                stronzo = true
            }
            if(maxValue > 50){
                maxValue = 50
                stronzo = true
            }

            const results = [];

            for(let i = 0; i < nDices; ++i){
                results.push(_.random(1, maxValue));
            }

            this.channel.send(`<@${this.author.id}> ha tirato: ${results.join(', ')}${stronzo ? ` (Volevi fare lo stronzo eh? Il limite per n. dadi e valore è 50)` : ''}`);
        }

    }

}

module.exports = Roll;
